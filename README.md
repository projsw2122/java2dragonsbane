# DragonsBane (Java version)

Written for the "Software Design" course of M.EEC, in FEUP, Portugal

Main contributers: Ricardo Sousa
- Ricardo B. Sousa [email](mailto:rbs@fe.up.pt)
- Armando Sousa    [email](mailto:asousa@fe.up.pt)

Text and plot of Quest: 
- Nuno Flores [email](nflores@fe.up.pt)

## License

[GPL v3](https://www.gnu.org/licenses/gpl-3.0.html)

In short, free software: Useful, transparent, no warranty


## Description

The mini-quest "Dragon's Bane" is supposed to reuse the results from mini-quest
I "Here Be Dragons". Concepts such as classes and objects should be used for
implementing the mini-quest. This mini-quest is implemented in Java and was
compiled in VS Code. See the [`guide`](doc/MiniQuest2DragonsBane.md)
for more information about the mini-quest.

Only the common part of the mini-quest is implemented in Java. The videos
available are the following ones:

- Package separation ([video](https://www.youtube.com/watch?v=ob-8gabEB_M))
- New Game Logic ([video](https://www.youtube.com/watch?v=GcybhYNrOo8))
- Dragon movement and multiple dragons
  ([video](https://www.youtube.com/watch?v=TfEQHLTe3_E))

## Usage

### VS Code

1. Open `src/DragonsBane.java`
2. Run `DragonsBane` project
   - VS Code > Run > Start debugging (`F5`)
   - OR VS Code > Run > Run without debugging (`Ctrl+F5`)


